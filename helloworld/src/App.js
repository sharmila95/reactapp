import React from 'react';
import './App.css';
//import Greet from './components/Greet' //For default export

import { Greet } from './components/Greet'; //For name Export
import Welcome from './components/Welcome';
import Grid from './components/Grid';
import Hello from './components/Sample';
import Bind from './components/Databinding';
import Hooks from './components/Hooks';

function App() {
  return (
    <div className="App">
        <Greet />
        <Grid> </Grid>
        <Hello />
        <Bind></Bind>
        <Hooks></Hooks>        
    </div>
  );
}

export default App;
