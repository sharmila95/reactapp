import React from 'react'

//Normal way of writing 
// function Greet() {
//     return <h1>Hello People</h1>
// }

//Functional Component using Arrow functions

// const Greet = () => <h1>Hello People</h1>

//Name Export
export const Greet = () => <h1>Hello People</h1>


// export default Greet