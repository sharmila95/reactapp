import  React, { Component } from 'react';
import Edit from '@syncfusion/ej2-react-grids';

import { AgGridReact } from 'ag-grid-react';

import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import 'ag-grid-enterprise';

class Grid extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            columnDefs: [
                { headerName: "Make", field: "make", sortable:true, filter: true, rowGroup: true}, //checkboxSelection: true
                { headerName: "Model", field: "model", sortable:true, filter: true },
                { headerName: "Price", field: "price", sortable:true, filter: true }
            ],
            rowData: null,
            // rowData: [
            //     { make: "Toyota", model: "Celica", price: 35000 },
            //     { make: "Ford", model: "Mondeo", price: 32000 },
            //     { make: "Porsche", model: "Boxter", price: 72000 }]

            autoGroupColumnDef: {
                headerName: "Model",
                field: "model",
                cellRenderer:'agGroupCellRenderer',
                cellRendererParams: {
                  checkbox: true
                }
              }
        };
    }



    componentDidMount() {
        fetch('https://api.myjson.com/bins/15psn9')
            .then(res => res.json())
            .then(rowData => this.setState({rowData}))
            .catch(err => console.log(err));
    }

    onButtonClick = () => {
        const selectedNodes = this.gridApi.getSelectedNodes();
        const selectedData = selectedNodes.map(node => node.data);
        const selectedDataStringPresentation = selectedData.map(node => node.make + ' ' + node.model).join(', ');
        alert(`Selected Nodes: ${selectedDataStringPresentation}`);
    }
    render() {
        return (
            <div className="ag-theme-balham" 
                style={ {
                    height: '600px', width: '600px', display: 'inline-block'
                } }>
                    
                <button style={{marginBottom: '10px'}}onClick={this.onButtonClick}>Get Selected Rows</button>
                
                <AgGridReact
                    columnDefs={this.state.columnDefs}
                    autoGroupColumnDef={this.state.autoGroupColumnDef}
                    rowData={this.state.rowData}
                    rowSelection="multiple"
                    onGridReady={params => this.gridApi = params.api}
                    groupSelectsChildren={true}
                />
            </div>
        )
    }
}

export default Grid;
