import React  from 'react';
import { render } from 'react-dom';
class Bind extends React.Component {
    constructor() {
      super();
      this.state = {value : ''}
    }
    handleChange = (e) =>{ 
      this.setState({value: e.target.value});
    }
    render() {
      return (
      <div>
          <input type="text" value={this.state.value} onChange={this.handleChange}/>
          <div>{this.state.value}</div>
      </div>
     )
    }
  }
export default Bind