import React from 'react'

const Hello = () => {
    
    //With jsx
    return (
        <div className='dummyClass'>
            <h1>Hello JSX</h1>
        </div>
    )

    //Without jsx
    // return React.createElement('div', {id: 'hello', className: 'dummyClass'}, React.createElement('h1', null, 'Hello JSX' ))
}

export default Hello